export const sliderItems = [
  {
    id: 1,
    img: "/photo1nobg.png",
    title: "UP TO 30% OFF",
    desc: "DON'T COMPROMISE ON STYLE! GET FLAT 30% OFF FOR NEW ARRIVALS.",
    bg: "fcf1ed",
  },
  {
    id: 2,
    img: "/photo2nobg.png",
    title: "POPULAR CHOICE",
    desc: "Loved by the public, bought by you",
    bg: "f5fafd",
  },
  {
    id: 3,
    img: "photo3nobg.png",
    title: "BEST BUY",
    desc: "The best offer you could possible get",
    bg: "fbf0f4",
  },
];

export const popularProducts = [
  {
    id: 1,
    img: "/photo1.jpg",
  },
  {
    id: 2,
    img: "/photo2.jpg",
  },
  {
    id: 3,
    img: "/photo3.jpg",
  },
  {
    id: 4,
    img: "/photo4.jpg",
  },
  {
    id: 5,
    img: "/photo5.jpg",
  },
  {
    id: 6,
    img: "/photo6.jpg",
  },
];

import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div``;

const App = () => {
  return (
    <Container>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} exact></Route>
          <Route path="/login" element={<Login />} exact></Route>
          <Route path="/register" element={<Register />}></Route>
        </Routes>
      </Router>
    </Container>
  );
};

export default App;

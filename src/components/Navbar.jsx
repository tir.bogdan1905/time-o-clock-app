import React from "react";
import styled from "styled-components";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import { Badge } from "@mui/material";

const Container = styled.div`
  height: 60px;
`;

const Wrapper = styled.div`
  padding: 10px 20px;
  display: flex;
  justify-content: space-between;
`;

const Left = styled.div`
  width: 50%;
  text-align: start;
  margin-left: 20px;
`;

const Logo = styled.h1`
  font-weight: bold;
`;

const Right = styled.a`
  width: 50%;
  text-align: end;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const MenuItem = styled.a`
  font-size: 14px;
  cursor: pointer;
  margin-left: 25px;
  text-decoration: none;
  color: black;
`;

function Navbar() {
  return (
    <Container>
      <Wrapper>
        <Left>
          <Logo>Time o'clock</Logo>
        </Left>

        <Right>
          <MenuItem href="/register">Register</MenuItem>
          <MenuItem href="/login">Sign in</MenuItem>
          <MenuItem>
            <Badge badgeContent={4} color="primary">
              <ShoppingBagOutlinedIcon />
            </Badge>
          </MenuItem>
        </Right>
      </Wrapper>
    </Container>
  );
}

export default Navbar;

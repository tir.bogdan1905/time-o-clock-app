import styled from "styled-components";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import TwitterIcon from "@mui/icons-material/Twitter";
import RedditIcon from "@mui/icons-material/Reddit";
import MapIcon from "@mui/icons-material/Map";
import PhoneIcon from "@mui/icons-material/Phone";
import MailIcon from "@mui/icons-material/Mail";

const Container = styled.div`
  background-color: #ebebeb;
  display: flex;
  justify-content: space-around;
`;
const Left = styled.div`
  flex: 1;
  display: flex;
  margin: 20px 20px 20px 40px;
  flex-direction: column;
`;

const Logo = styled.h1``;

const Desc = styled.p`
  font-size: 20px;
  margin: 20px 0px;
  text-align: justify;
`;

const SocialContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const SocialIcon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50px;
  color: white;
  background-color: #${(props) => props.color};
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 20px;
  cursor: pointer;
`;
const Center = styled.div`
  margin: 20px;
  font-size: 20px;
  display: flex;
  flex: 1;
  text-align: center;
  justify-content: center;
  flex-direction: column;
`;

const Quote = styled.h2`
  font-style: italic;
`;

const Right = styled.div`
  font-size: 20px;
  margin: 20px;
  flex: 1;
  padding: 20px;
  align-items: end;
`;

const Title = styled.h3`
  margin-bottom: 30px;
`;

const ContactItem = styled.div`
  display: flex;
  padding: 0px;
  align-items: center;
`;

function Footer() {
  return (
    <Container>
      <Left>
        <Logo>Time o'clock</Logo>
        <Desc>
          Have you ever dreamed about future? Have you ever regreted the past?
          Have you ever wanted to control the time? Well this is imposible. But
          what is posible is to get a trully special watch to help you visualize
          the inexorable passage of time. And this is what time o'clock is
          about. Get yourself that special gift and start spending your life in
          the most intentional way because life is to short to miss it.
        </Desc>
      </Left>
      <Center>
        <Title>Remember this!</Title>
        <Quote>
          Time is free, but it's priceless. You can't own it, but you can use
          it. You can't keep it, but you can spend it. Once you've lost it you
          can never get it back. <br />
          -Harvey Mackay-
        </Quote>
        <SocialContainer>
          <SocialIcon color="3B5999">
            <FacebookIcon />
          </SocialIcon>
          <SocialIcon color="E4405F">
            <InstagramIcon />
          </SocialIcon>
          <SocialIcon color="55ACEE">
            <TwitterIcon />
          </SocialIcon>
          <SocialIcon color="FF4500">
            <RedditIcon />
          </SocialIcon>
        </SocialContainer>
      </Center>
      <Right>
        <Title>Contact</Title>
        <ContactItem>
          <MapIcon style={{ marginRight: "10px" }} /> Berarie, Medgidia, Romania
        </ContactItem>
        <ContactItem>
          <PhoneIcon style={{ marginRight: "10px" }} /> +40738549013
        </ContactItem>
        <ContactItem>
          <MailIcon style={{ marginRight: "10px" }} /> tir.bogdan1905@gmail.com
        </ContactItem>
      </Right>
    </Container>
  );
}

export default Footer;
